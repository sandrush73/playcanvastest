ASSET_PREFIX = "";
SCRIPT_PREFIX = "";
SCENE_PATH = "1266903.json";
CONTEXT_OPTIONS = {
    'antialias': true,
    'alpha': true,
    'preserveDrawingBuffer': false,
    'preferWebGl2': true,
    'powerPreference': "default"
};
SCRIPTS = [ 59669309, 59669317, 59669318, 59669332, 59669441, 59669339, 59670106, 59669327, 59672043, 59669513, 59669396, 59669413, 59669424, 59669450, 59670107, 59669428, 60032283 ];
CONFIG_FILENAME = "config.json";
INPUT_SETTINGS = {
    useKeyboard: true,
    useMouse: true,
    useGamepads: false,
    useTouch: true
};
pc.script.legacy = false;
PRELOAD_MODULES = [
    {'moduleName' : 'Ammo', 'glueUrl' : 'files/assets/59669449/1/ammo.wasm.js', 'wasmUrl' : 'files/assets/59669506/1/ammo.wasm.wasm', 'fallbackUrl' : 'files/assets/59669313/1/ammo.js', 'preload' : true},
];
